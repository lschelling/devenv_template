# README #

### Pre-requisites ###

* Vagrant >1.7 
* VirtualBox

### What is this repository for? ###

This is a template for a development environment using Vagrant and Docker.

    $ git clone https://lschelling@bitbucket.org/lschelling/devenv_template.git
    $ cd devenv_template
    $ vagrant up --provider docker
	$ vagrant ssh





